package de.derfrzocker.testplugin;

import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.EntityType;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;
import java.util.UUID;

public class TestBlockPopulator extends BlockPopulator {

    static TestBlockPopulator INSTANCE = new TestBlockPopulator();

    private final int MAX_CHUNKS = 30;

    private volatile int chunks = 0;

    @Override
    public boolean isParallelCapable() {
        return true;
    }

    @Override
    public void populate(World world, Random random, Chunk source) {
        if (MAX_CHUNKS <= chunks)
            return;

        populate0(world, random, source);
        chunks++;
    }


    private void populate0(World world, Random random, Chunk source) {
        {// testing normal block placing with source Chunk
            for (int x = 0; x < 16; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 5; y++)
                        source.getBlock(x, y + 70, z).setType(Material.GREEN_STAINED_GLASS, false);
        }

        {// testing normal block placing with new Location -> getBlock
            for (int x = 0; x < 16; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 5; y++)
                        new Location(world, x + (source.getX() << 4), y + 80, z + (source.getZ() << 4)).getBlock().setType(Material.RED_STAINED_GLASS, false);
        }

        {// testing normal block placing with new Location -> getChunk -> getBlock
            for (int x = 0; x < 16; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 5; y++)
                        new Location(world, x + (source.getX() << 4), y + 90, z + (source.getZ() << 4)).getChunk().getBlock(x, y + 90, z).setType(Material.ORANGE_STAINED_GLASS, false);
        }

        {// testing normal block placing with given world -> getBlockAt
            for (int x = 0; x < 16; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 5; y++)
                        world.getBlockAt(x + (source.getX() << 4), y + 100, z + (source.getZ() << 4)).setType(Material.PURPLE_STAINED_GLASS, false);
        }

        {// testing normal block placing with given world -> getChunk -> getBlock
            for (int x = 0; x < 16; x++)
                for (int z = 0; z < 16; z++)
                    for (int y = 0; y < 5; y++)
                        world.getChunkAt(source.getX(), source.getZ()).getBlock(x, y + 110, z).setType(Material.PURPLE_STAINED_GLASS, false);
        }

        {// testing normal tree Generation
            source.getBlock(0, 119, 0).setType(Material.DIRT, false);
            world.generateTree(source.getBlock(0, 120, 0).getLocation(), TreeType.BIG_TREE);
        }

        {// testing tree Generation with BlockChangeDelegate // TODO not working as indented
            source.getBlock(0, 199, 0).setType(Material.DIRT, false);
            world.generateTree(source.getBlock(0, 200, 0).getLocation(), TreeType.BIG_TREE, new BlockChangeDelegate() {
                @Override
                public boolean setBlockData(int x, int y, int z, BlockData blockData) {
                    System.out.println("generate: x: " + x + " y: " + y + " z: " + z + " material: " + blockData.getMaterial());

                    if (y < 210)
                        return false;

                    world.getBlockAt(x, y, z).setType(blockData.getMaterial());

                    return true;
                }

                @Override
                public BlockData getBlockData(int x, int y, int z) {
                    return null;
                }

                @Override
                public int getHeight() {
                    return 0;
                }

                @Override
                public boolean isEmpty(int x, int y, int z) {
                    return false;
                }
            });
        }

        {
            for (int x = 0; x < 16; x++)
                for (int z = 0; z < 16; z++)
                    world.getChunkAt(source.getX(), source.getZ()).getBlock(x, 149, z).setType(Material.STONE, false);
        }

        { // testing normal BlockState
            Block block = world.getBlockAt((source.getX() << 4) + 1, 150, (source.getZ() << 4) + 1);
            block.setType(Material.STONE);
            BlockState blockState = block.getState();
            blockState.setType(Material.SANDSTONE);
            blockState.update(true, false);
        }

        { // testing TileEntity: Sign
            Block block = world.getBlockAt((source.getX() << 4) + 2, 150, (source.getZ() << 4) + 1);
            block.setType(Material.SPRUCE_SIGN);
            Sign sign = (Sign) block.getState();
            sign.setLine(0, "ping");
            sign.setLine(1, "...");
            sign.setLine(2, "pong");
            sign.update(true, false);
        }

        { // testing TileEntity: Chest
            Block block = world.getBlockAt((source.getX() << 4) + 3, 150, (source.getZ() << 4) + 1);
            block.setType(Material.CHEST);
            Chest chest = (Chest) block.getState();
            chest.getSnapshotInventory().addItem(new ItemStack(Material.CHEST), new ItemStack(Material.STONE));
            chest.update(true, false);
        }

        { // testing TileEntity: Furnace
            Block block = world.getBlockAt((source.getX() << 4) + 4, 150, (source.getZ() << 4) + 1);
            block.setType(Material.FURNACE);
            Furnace furnace = (Furnace) block.getState();
            furnace.getSnapshotInventory().setSmelting(new ItemStack(Material.DIAMOND_ORE, 32));
            furnace.getSnapshotInventory().setFuel(new ItemStack(Material.COAL_BLOCK, 32));
            furnace.update(true, false);
        }

        { // testing TileEntity: Dispencer
            Block block = world.getBlockAt((source.getX() << 4) + 5, 150, (source.getZ() << 4) + 1);
            block.setType(Material.DISPENSER);
            Dispenser dispenser = (Dispenser) block.getState();
            dispenser.getSnapshotInventory().addItem(new ItemStack(Material.ARROW, 32), new ItemStack(Material.SNOWBALL, 32));
            dispenser.update(true, false);
        }

        { // testing TileEntity: Dropper
            Block block = world.getBlockAt((source.getX() << 4) + 6, 150, (source.getZ() << 4) + 1);
            block.setType(Material.DROPPER);
            Dropper dropper = (Dropper) block.getState();
            dropper.getSnapshotInventory().addItem(new ItemStack(Material.ARROW, 32), new ItemStack(Material.SNOWBALL, 8));
            dropper.update(true, false);
        }

        { // testing TileEntity: EndGateway
            Block block = world.getBlockAt((source.getX() << 4) + 7, 150, (source.getZ() << 4) + 1);
            block.setType(Material.END_GATEWAY);
            EndGateway endGateway = (EndGateway) block.getState();
            endGateway.setAge(234789);
            endGateway.setExitLocation(new Location(Bukkit.getWorld(world.getUID()), 0, 100, 0));
            endGateway.update(true, false);
        }

        { // testing TileEntity: Hopper
            Block block = world.getBlockAt((source.getX() << 4) + 8, 150, (source.getZ() << 4) + 1);
            block.setType(Material.HOPPER);
            Hopper hopper = (Hopper) block.getState();
            hopper.getSnapshotInventory().addItem(new ItemStack(Material.ACACIA_LOG, 16), new ItemStack(Material.ACACIA_LEAVES, 8));
            hopper.update(true, false);
        }

        { // testing TileEntity: CreatureSpawner
            Block block = world.getBlockAt((source.getX() << 4) + 9, 150, (source.getZ() << 4) + 1);
            block.setType(Material.SPAWNER);
            CreatureSpawner creatureSpawner = (CreatureSpawner) block.getState();
            creatureSpawner.setSpawnedType(EntityType.ENDERMITE);
            creatureSpawner.update(true, false);
        }

        { // testing TileEntity: Jukebox
            Block block = world.getBlockAt((source.getX() << 4) + 10, 150, (source.getZ() << 4) + 1);
            block.setType(Material.JUKEBOX);
            Jukebox jukebox = (Jukebox) block.getState();
            jukebox.setRecord(new ItemStack(Material.MUSIC_DISC_WAIT));
            jukebox.update(true, false);
        }

        { // testing TileEntity: BrewingStand
            Block block = world.getBlockAt((source.getX() << 4) + 11, 150, (source.getZ() << 4) + 1);
            block.setType(Material.BREWING_STAND);
            BrewingStand brewingStand = (BrewingStand) block.getState();
            brewingStand.setFuelLevel(4);
            brewingStand.getSnapshotInventory().setIngredient(new ItemStack(Material.GUNPOWDER));
            brewingStand.update(true, false);
        }

        { // testing TileEntity: Skull
            Block block = world.getBlockAt((source.getX() << 4) + 12, 150, (source.getZ() << 4) + 1);
            block.setType(Material.PLAYER_HEAD);
            Skull skull = (Skull) block.getState();
            skull.setOwningPlayer(Bukkit.getOfflinePlayer(UUID.fromString("29a4042b-05ab-4c72-9460-7aa3b567e8da")));
            skull.update(true, false);
        }

        { // testing TileEntity: CommandBlock
            Block block = world.getBlockAt((source.getX() << 4) + 13, 150, (source.getZ() << 4) + 1);
            block.setType(Material.COMMAND_BLOCK);
            CommandBlock commandBlock = (CommandBlock) block.getState();
            commandBlock.setName("ping");
            commandBlock.setCommand("say pong");
            commandBlock.update(true, false);
        }

        { // testing TileEntity: Beacon
            Block block = world.getBlockAt((source.getX() << 4) + 14, 150, (source.getZ() << 4) + 1);
            block.setType(Material.BEACON);
            Beacon beacon = (Beacon) block.getState();
            beacon.setPrimaryEffect(PotionEffectType.INCREASE_DAMAGE);
            beacon.update(true, false);
        }

        { // testing TileEntity: Banner
            Block block = world.getBlockAt((source.getX() << 4) + 1, 150, (source.getZ() << 4) + 2);
            block.setType(Material.BLACK_BANNER);
            Banner banner = (Banner) block.getState();
            banner.setBaseColor(DyeColor.GRAY);
            banner.addPattern(new Pattern(DyeColor.WHITE, PatternType.CROSS));
            banner.update(true, false);
        }

        { // testing TileEntity: StructureBlock,
            Block block = world.getBlockAt((source.getX() << 4) + 2, 150, (source.getZ() << 4) + 2);
            block.setType(Material.STRUCTURE_BLOCK);
            Structure structure = (Structure) block.getState();
            //structure.setUsageMode(UsageMode.CORNER);//TODO not working, nullpointer since World is null in
            structure.update(true, false);
        }

        { // testing TileEntity: ShulkerBox
            Block block = world.getBlockAt((source.getX() << 4) + 3, 150, (source.getZ() << 4) + 2);
            block.setType(Material.SHULKER_BOX);
            ShulkerBox shulkerBox = (ShulkerBox) block.getState();
            shulkerBox.getSnapshotInventory().addItem(new ItemStack(Material.ARMOR_STAND), new ItemStack(Material.BONE, 64));
            shulkerBox.update(true, false);
        }

        { // testing TileEntity: EnchantingTable
            Block block = world.getBlockAt((source.getX() << 4) + 4, 150, (source.getZ() << 4) + 2);
            block.setType(Material.ENCHANTING_TABLE);
            EnchantingTable enchantingTable = (EnchantingTable) block.getState();
            enchantingTable.setCustomName("Ping pong");
            enchantingTable.update(true, false);
        }

        { // testing TileEntity: EnderChest
            Block block = world.getBlockAt((source.getX() << 4) + 5, 150, (source.getZ() << 4) + 2);
            block.setType(Material.ENDER_CHEST);
            EnderChest enderChest = (EnderChest) block.getState();
            enderChest.update(true, false);
        }

        { // testing TileEntity: DaylightDetector
            Block block = world.getBlockAt((source.getX() << 4) + 6, 150, (source.getZ() << 4) + 2);
            block.setType(Material.DAYLIGHT_DETECTOR);
            DaylightDetector daylightDetector = (DaylightDetector) block.getState();
            daylightDetector.update(true, false);
        }

        { // testing TileEntity: Comparator
            Block block = world.getBlockAt((source.getX() << 4) + 7, 150, (source.getZ() << 4) + 2);
            block.setType(Material.COMPARATOR);
            Comparator comparator = (Comparator) block.getState();
            comparator.update(true, false);
        }

        { // testing TileEntity: Bed
            Block block = world.getBlockAt((source.getX() << 4) + 8, 150, (source.getZ() << 4) + 2);
            block.setType(Material.BLACK_BED);
            Bed bed = (Bed) block.getState();
            bed.update(true, false);
        }

        { // testing TileEntity: Conduit
            Block block = world.getBlockAt((source.getX() << 4) + 9, 150, (source.getZ() << 4) + 2);
            block.setType(Material.CONDUIT);
            Conduit conduit = (Conduit) block.getState();
            conduit.update(true, false);
        }

        { // testing TileEntity: Barrel
            Block block = world.getBlockAt((source.getX() << 4) + 10, 150, (source.getZ() << 4) + 2);
            block.setType(Material.BARREL);
            Barrel barrel = (Barrel) block.getState();
            barrel.getSnapshotInventory().addItem(new ItemStack(Material.BOOK, 16), new ItemStack(Material.DIORITE, 64));
            barrel.update(true, false);
        }

        { // testing TileEntity: Bell
            Block block = world.getBlockAt((source.getX() << 4) + 11, 150, (source.getZ() << 4) + 2);
            block.setType(Material.BELL);
            BlockState bell = block.getState(); // CraftBell don't impl Bell ?
            bell.update(true, false);
        }

        { // testing TileEntity: BlastFurnace
            Block block = world.getBlockAt((source.getX() << 4) + 12, 150, (source.getZ() << 4) + 2);
            block.setType(Material.BLAST_FURNACE);
            BlastFurnace blastFurnace = (BlastFurnace) block.getState();
            blastFurnace.getSnapshotInventory().setFuel(new ItemStack(Material.CHARCOAL, 32));
            blastFurnace.getSnapshotInventory().setSmelting(new ItemStack(Material.IRON_ORE, 64));
            blastFurnace.update(true, false);
        }

        { // testing TileEntity: Campfire
            Block block = world.getBlockAt((source.getX() << 4) + 13, 150, (source.getZ() << 4) + 2);
            block.setType(Material.CAMPFIRE);
            Campfire campfire = (Campfire) block.getState();
            campfire.setItem(0, new ItemStack(Material.BEEF));
            campfire.update(true, false);
        }

        { // testing TileEntity: Jigsaw
            Block block = world.getBlockAt((source.getX() << 4) + 14, 150, (source.getZ() << 4) + 2);
            block.setType(Material.JIGSAW);
            Jigsaw jigsaw = (Jigsaw) block.getState();
            jigsaw.update(true, false);
        }

        { // testing TileEntity: Lectern
            Block block = world.getBlockAt((source.getX() << 4) + 1, 150, (source.getZ() << 4) + 3);
            block.setType(Material.LECTERN);
            Lectern lectern = (Lectern) block.getState();
            //lectern.getSnapshotInventory().addItem(new ItemStack(Material.WRITTEN_BOOK)); // TODO cause a NullPointerException
            lectern.update(true, false);
        }

        { // testing TileEntity: Smoker
            Block block = world.getBlockAt((source.getX() << 4) + 2, 150, (source.getZ() << 4) + 3);
            block.setType(Material.SMOKER);
            Smoker smoker = (Smoker) block.getState();
            smoker.getSnapshotInventory().setFuel(new ItemStack(Material.COAL, 64));
            smoker.getSnapshotInventory().setSmelting(new ItemStack(Material.CHICKEN, 32));
            smoker.update(true, false);
        }

        { // Placing Block in Chunk nearby: no problems
            Block block = world.getBlockAt((source.getX() << 4) - 10, 200, (source.getZ() << 4) - 1);
            block.setType(Material.STONE);
        }
    }

}
